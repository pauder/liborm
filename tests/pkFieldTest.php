<?php

require_once dirname(__FILE__).'/fieldTest.php';

class ORM_pkFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_PkField';






    /**
     * Test primary key with zero value
     */
    public function testMockNoAutoIncrement()
    {
        $backend = new ORM_MySqlMockBackend(new LibOrm_MysqlMockAdapter);
        ORM_RecordSet::setBackend($backend);

        $set = new ORM_TestRecordSet();
        $set->addFields(
            ORM_PkField('test')->setAutoIncrement(false)
        );
        $set->setPrimaryKey('test');

        $injectRecord = $set->newRecord();

        $backend->setSelectReturn($set, $set->test->is(0), array($injectRecord));
        $testRecord = $set->get(0);

        $this->assertInstanceOf('ORM_TestRecord', $testRecord);
    }




    /**
     * Test primary key as string
     */
    public function testMockWithString()
    {
        $backend = new ORM_MySqlMockBackend(new LibOrm_MysqlMockAdapter);
        ORM_RecordSet::setBackend($backend);

        $set = new ORM_TestRecordSet();
        $set->addFields(
            ORM_PkStringField('test')
        );
        $set->setPrimaryKey('test');

        $injectRecord = $set->newRecord();

        $backend->setSelectReturn($set, $set->test->is('abcd'), array($injectRecord));
        $testRecord = $set->get('abcd');

        $this->assertInstanceOf('ORM_TestRecord', $testRecord);
    }
}
