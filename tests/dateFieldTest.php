<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

class ORM_dateFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_DateField';


    public function testIsValueSetWithNullAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(true);

        $this->assertFalse($field->isValueSet(null));
        $this->assertTrue($field->isValueSet(''));
        $this->assertTrue($field->isValueSet(false));
        $this->assertTrue($field->isValueSet(0));
        $this->assertTrue($field->isValueSet(ORM_DatetimeField::EMPTY_DATETIME));
        $this->assertTrue($field->isValueSet(ORM_DateField::EMPTY_DATE));
    }

    public function testIsValueSetWithNullNotAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(false);

        $this->assertFalse($field->isValueSet(null), 'Null value is not set');
        $this->assertFalse($field->isValueSet(''), 'an empty string is not a set date if null value not allowed');
        $this->assertFalse($field->isValueSet(false));
        $this->assertFalse($field->isValueSet(ORM_DatetimeField::EMPTY_DATETIME), 'this type of error should be supported');
        $this->assertFalse($field->isValueSet(ORM_DateField::EMPTY_DATE));
        $this->assertTrue($field->isValueSet('2015-01-01 00:00:00'), 'this type of error should be supported');
        $this->assertTrue($field->isValueSet('2015-01-01'));
    }

    public function testInputFromIsoFormattedDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('2015-12-31');
        $this->assertEquals($value, '2015-12-31');
    }

    public function testInputFromFrenchFormattedDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('31-12-2015');
        $this->assertEquals($value, '2015-12-31');
    }

    public function testInputFromFrenchFormattedShortDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('31-12-15');
        $this->assertEquals($value, '2015-12-31');

        $value = $field->input('31-12-99');
        $this->assertEquals($value, '2099-12-31');

        $value = $field->input('31-12-00');
        $this->assertEquals($value, '2000-12-31');
    }

    public function testInputFromBadDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('BAD DATE');
        $this->assertEquals($value, ORM_DateField::EMPTY_DATE);
    }

    public function testNullInputWithNullAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(true);

        $value = $field->input(null);
        $this->assertNull($value);
    }

    public function testNullInputWithNullNotAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(false);

        $value = $field->input(null);
        $this->assertEquals($value, ORM_DateField::EMPTY_DATE);
    }
}
