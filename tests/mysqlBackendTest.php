<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/backendTest.php';

class ORM_mysqlBackendTest extends ORM_backendTest
{
    /**
     * Test save query for a new record with one modified field.
     */
    public function testRecordSaveQuery()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);

        $record = $recordSet->newRecord();
        $record->name = 'myName';

        $sql = $recordSet->getSaveQuery($record);
        $this->assertEquals(
            $sql,
            "INSERT INTO orm_testrecord (`name`)
VALUES ('myName')"
        );
    }


    /**
     * Test save query for a new record with one modified field and a primary key.
     */
    public function testRecordSaveQueryWithPrimaryKey()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->setPrimaryKey('id');
        $recordSet->addFields($field1, $field2);

        $record = $recordSet->newRecord();
        $record->name = 'myName';

        $sql = $recordSet->getSaveQuery($record);

        $this->assertEquals(
            $sql,
            "INSERT INTO orm_testrecord (`name`)
VALUES ('myName')"
        );
    }


    /**
     * Test save query for a new record with one modified field and a modified primary key.
     */
    public function testRecordSaveQueryWithModifiedPrimaryKey()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->setPrimaryKey('id');
        $recordSet->addFields($field1, $field2);

        $record = $recordSet->newRecord();
        $record->id = 12;
        $record->name = 'myName';

        $sql = $recordSet->getSaveQuery($record);
        $sql = str_replace("\n", ' ', $sql);
//        var_dump($sql);

        $this->assertEquals(
            $sql,
            "INSERT INTO orm_testrecord (`id`, `name`) VALUES ('12', 'myName')"
        );
    }



    /**
     * The 'modified' status of a new record is false.
     */
    public function testNewRecordIsNotModified()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
        $record = $recordSet->newRecord();

        $this->assertFalse(
            $record->isModified()
        );
    }


    /**
     * A property value set with __set affects
     * the 'modified' status of the record.
     */
    public function testRecordWithModifiedPropertyUsingMagicSetIsModified()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
        $record = $recordSet->newRecord();
        $record->name = 'my name 1';

        $this->assertTrue(
            $record->isModified()
        );
    }

    /**
     * A property value set with __set affects
     * the 'modified' status of the record.
     */
    public function testRecordWithModifiedPropertyUsingSetValueIsModified()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
        $record = $recordSet->newRecord();
         $record->setValue('name', 'my name 2');

        $this->assertTrue(
            $record->isModified()
        );
    }


    /**
     * A property value set with initValue() does not affect
     * the 'modified' status of the record.
     */
    public function testRecordWithModifiedPropertyUsinInitValueIsNotModified()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
        $record = $recordSet->newRecord();
        $record->initValue('name', 'my name 3');

        $this->assertFalse(
            $record->isModified()
        );
    }


    /**
     * If you set the same value on an initilized property the record
     * is considered modified anyway. (Is it good ?).
     */
    public function testRecordWithInitializedThenModifiedWithSameValuePropertyIsModified()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
        $record = $recordSet->newRecord();
        $record->initValue('name', 'my name 4');
        $record->name = 'my name 4';

        $this->assertTrue(
            $record->isModified()
        );
    }


    public function testGetTableName()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));

        $tableName = $recordSet->getTableName();


        $this->assertEquals(
            $tableName,
            'orm_testrecord'
        );
    }




    protected function assertGetCriteria($pkvalue)
    {
        $db = new LibOrm_MysqlMockAdapter();
        $mockBackend = new ORM_MySqlMockBackend($db);

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setPrimaryKey('id');
        $recordSet->setBackend($mockBackend);

        $recordSet->get($pkvalue);
        $criteria = $mockBackend->getLastSelectCriteria($recordSet);

        $this->assertInstanceOf('ORM_Criteria', $criteria);
        $this->assertInstanceOf('ORM_IsCriterion', $criteria);
        $this->assertEquals($pkvalue, $criteria->getValue());
    }



    /**
     * Test the get method with null id value
     */
    public function testSetGetWithNullId()
    {
        $this->assertGetCriteria(null);
    }


    /**
     * Test the get method with false id value
     */
    public function testSetGetWithFalseId()
    {
        $this->assertGetCriteria(false);
    }

    /**
     * Test the get method with empty string or zero id value
     */
    public function testSetGetWithEmptyId()
    {
        $this->assertGetCriteria('');
        $this->assertGetCriteria(0);
    }


    protected function assertIsSql($expectedSql, $value)
    {
        $db = new LibOrm_MysqlMockAdapter();
        $backend = new ORM_MySqlBackend($db);

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setPrimaryKey('id');
        $field = $recordSet->getField('id');

        if (is_array($value)) {
            $sql = $backend->in($field, $value);
        } else {
            $sql = $backend->is($field, $value);
        }

        $alias = $backend->getFieldAlias($field);

        $this->assertEquals($alias.$expectedSql, $sql);
    }


    public function testSqlConversion()
    {
        $this->assertIsSql(" = '1'", 1);
        $this->assertIsSql(" = '1'", '1');
        $this->assertIsSql(" = '-1'", -1);
        $this->assertIsSql(" = '-1'", '-1');
        $this->assertIsSql(" = '0.569'", 0.569);
        $this->assertIsSql(" = '0,569'", '0,569');
        $this->assertIsSql(" = 'text'", 'text');
        $this->assertIsSql(" = '0'", false);
        $this->assertIsSql(" = '1'", true);
        $this->assertIsSql(" = ''", '');
        $this->assertIsSql(" IS NULL", null);

        $this->assertIsSql(" IN('1','-1','0')", array(1, -1, 0));
        $this->assertIsSql(" IN('1','-1','0')", array('1', '-1', '0'));
        $this->assertIsSql(" IN('0.569','-0.569','0,569')", array(0.569, -0.569, '0,569'));
        $this->assertIsSql(" IN('')", array(''));
    }

    public function testBooleanInValues()
    {
        $this->assertIsSql(" IN('1','0')", array(true, false));
    }

    /**
     * 
     */
    public function testInNullValues()
    {        
        $this->assertIsSql(" IN(NULL)", array(null));
    }


    public function assertWeekOperation($field, $value, $weekNumber)
    {
        // use backend on test database
        $babDB = new LibOrm_MysqlAdapter('localhost', 'test', '', 'test');
        $backend = new ORM_MySqlBackend($babDB);


        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend($backend);

        $recordSet->addFields($field);

        $weekOp = $field->week();
        /*@var $weekOp ORM_WeekOperation */
        $weekOp->setValue($value);
        $sql = $backend->weekOperation($weekOp);

        $res = $babDB->db_query('SELECT '.$sql.' AS w');
        $arr = $babDB->db_fetch_assoc($res);

        $sqlWeek = (int) $arr['w'];
        $phpWeek = date('W', bab_mktime($value));

        $this->assertEquals($sqlWeek, $weekNumber, 'test week number via mysql query');
        $this->assertEquals($phpWeek, $weekNumber, 'test if week number is the same using php');
    }


    public function testWeekOperation()
    {
        $datef = ORM_DateField('date');
        $dtime = ORM_DatetimeField('datetime');

        $this->assertWeekOperation($datef, '2015-01-01', 1);
        $this->assertWeekOperation($dtime, '2015-01-01 23:00:00', 1);

        $this->assertWeekOperation($datef, '2015-01-04', 1);
        $this->assertWeekOperation($dtime, '2015-01-04 23:00:00', 1);

        $this->assertWeekOperation($datef, '2015-01-05', 2);
        $this->assertWeekOperation($dtime, '2015-01-05 00:00:00', 2);

        $this->assertWeekOperation($datef, '2015-10-28', 44);
        $this->assertWeekOperation($dtime, '2015-10-28 12:00:00', 44);
    }
}
