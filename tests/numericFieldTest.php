<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

abstract class ORM_numericFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_NumericField';
    
    
    public function testOutputValuesAreConvertedCorrectly()
    {
        $field = $this->construct('numeric');
        
        $field->setDecimalSeparator(',');
        $field->setThousandsSeparator(' ');

        // Empty string yields zero
        $this->assertEquals('0,000', $field->output('', 3));
    
        // Non numeric string is returned unmodified.
        $this->assertEquals('0,000', $field->output('text', 3), 'Non numeric string are converted to 0 and formatted.');
    
        $this->assertEquals('1,123', $field->output(1.1234, 3));
        $this->assertEquals('1,124', $field->output(1.1235, 3));
        $this->assertEquals('1,124', $field->output(1.1236, 3));
    
        $this->assertEquals('2,123', $field->output('2.1234', 3));
        $this->assertEquals('2,124', $field->output('2.1235', 3));
        $this->assertEquals('2,124', $field->output('2.1236', 3));
    
        $this->assertEquals('-1,123', $field->output('-1.1234', 3));
        $this->assertEquals('-1,124', $field->output('-1.1235', 3));
        $this->assertEquals('-1,124', $field->output('-1.1236', 3));
    
        $this->assertEquals('2,000', $field->output('2', 3));
    
        $this->assertEquals('2 123,000', $field->output('2123', 3));
    
        $this->assertEquals('2 123,000', $field->output('2123.000', 3));

        $field->setDecimalSeparator('.');
        $field->setThousandsSeparator(',');

        $this->assertEquals('1.123', $field->output(1.1234, 3));
        $this->assertEquals('1.124', $field->output(1.1235, 3));
        $this->assertEquals('1.124', $field->output(1.1236, 3));
    
        $this->assertEquals('2.000', $field->output('2', 3));
    
        $this->assertEquals('2,123.000', $field->output('2123', 3));
    
        $this->assertEquals('2,123.000', $field->output('2123.000', 3));
    }
}
