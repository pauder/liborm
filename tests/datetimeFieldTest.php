<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

class ORM_datetimeFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_DatetimeField';


    public function testIsValueSetWithNullAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(true);

        $this->assertFalse($field->isValueSet(null));
        $this->assertTrue($field->isValueSet(''));
        $this->assertTrue($field->isValueSet(false));
        $this->assertTrue($field->isValueSet(0));
        $this->assertTrue($field->isValueSet(ORM_DatetimeField::EMPTY_DATETIME));
    }

    public function testIsValueSetWithNullNotAllowed()
    {
        $field = $this->construct('datetime');
        $field->setNullAllowed(false);

        $this->assertFalse($field->isValueSet(null), 'Null value is not set');
        $this->assertFalse($field->isValueSet(''), 'an empty string is not a set datetime if null value not allowed');
        $this->assertFalse($field->isValueSet(false));
        $this->assertFalse($field->isValueSet(ORM_DatetimeField::EMPTY_DATETIME));
        $this->assertFalse($field->isValueSet(ORM_DateField::EMPTY_DATE));
        $this->assertTrue($field->isValueSet('2015-01-01 00:00:00'));
    }

    public function testInputFromIsoFormattedDateTime()
    {
        $field = $this->construct('datetime');
        $value = $field->input('2015-12-31 11:35:50');
        $this->assertEquals($value, '2015-12-31 11:35:50');
    }

    public function testInputFromFrenchFormattedDateTime()
    {
        $field = $this->construct('datetime');
        $value = $field->input('31-12-2015 11:35:50');
        $this->assertEquals($value, '2015-12-31 11:35:50');

        // Short year
        $value = $field->input('31-12-15 11:35:50');
        $this->assertEquals($value, '2015-12-31 11:35:50');

        $value = $field->input('31-12-99 11:35:50');
        $this->assertEquals($value, '2099-12-31 11:35:50');

        $value = $field->input('31-12-00 11:35:50');
        $this->assertEquals($value, '2000-12-31 11:35:50');
    }

    public function testInputFromIsoFormattedDateTimeWithoutSeconds()
    {
        $field = $this->construct('datetime');
        $value = $field->input('2015-12-31 11:35');
        $this->assertEquals($value, '2015-12-31 11:35:00');
    }

    public function testInputFromFrenchFormattedDateTimeWithoutSeconds()
    {
        $field = $this->construct('datetime');
        $value = $field->input('31-12-2015 11:35');
        $this->assertEquals($value, '2015-12-31 11:35:00');

        // Short year
        $value = $field->input('31-12-15 11:35');
        $this->assertEquals($value, '2015-12-31 11:35:00');

        $value = $field->input('31-12-99 11:35');
        $this->assertEquals($value, '2099-12-31 11:35:00');

        $value = $field->input('31-12-00 11:35');
        $this->assertEquals($value, '2000-12-31 11:35:00');
    }

    public function testInputFromIsoFormattedDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('2015-12-31');
        $this->assertEquals($value, '2015-12-31 00:00:00');
    }

    public function testInputFromFrenchFormattedDate()
    {
        $field = $this->construct('datetime');
        $value = $field->input('31-12-2015');
        $this->assertEquals($value, '2015-12-31 00:00:00');

        // Short year
        $value = $field->input('31-12-15');
        $this->assertEquals($value, '2015-12-31 00:00:00');

        $value = $field->input('31-12-99');
        $this->assertEquals($value, '2099-12-31 00:00:00');

        $value = $field->input('31-12-00');
        $this->assertEquals($value, '2000-12-31 00:00:00');
     }

     public function testInputFromBadDate()
     {
         $field = $this->construct('datetime');
         $value = $field->input('BAD DATE');
         $this->assertEquals($value, ORM_DatetimeField::EMPTY_DATETIME);
     }

     public function testNullInputWithNullAllowed()
     {
         $field = $this->construct('datetime');
         $field->setNullAllowed(true);

         $value = $field->input(null);
         $this->assertNull($value);
     }

     public function testNullInputWithNullNotAllowed()
     {
         $field = $this->construct('datetime');
         $field->setNullAllowed(false);

         $value = $field->input(null);
         $this->assertEquals($value, ORM_DatetimeField::EMPTY_DATETIME);
     }
}
