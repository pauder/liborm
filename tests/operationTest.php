<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

abstract class ORM_operationTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_Operation';

    protected function getRecordSet()
    {
        global $babDB;

        $ORM = new Func_LibOrm;
        $ORM->initMysql();

        $babDB = new LibOrm_MysqlAdapter('localhost', 'test', '', 'test');
        $backend = new ORM_MySqlBackend($babDB);

        $set = new ORM_TestRecordCreateSet();
        $set->addFields($set->name->left(3)->setName('left3'));

        $set->setBackend($backend);

        return $set;
    }


    public function setUp()
    {
        global $babDB;
        $babDB->db_query('DROP TABLE IF EXISTS orm_testrecordcreate');

        $recordSet = $this->getRecordSet();

        require_once dirname(__FILE__).'/../vendor/ovidentia/ovidentia/ovidentia/utilit/devtools.php';

        $sql = new bab_synchronizeSql();
        $sql->addOrmSet($recordSet);

        $sql->updateDatabase();
    }

    public function tearDown()
    {
        global $babDB;
        $babDB->db_query('DROP TABLE IF EXISTS orm_testrecordcreate');
    }
}
