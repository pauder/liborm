<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';



class ORM_CriteriaTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test adding two ORM_Field to a record set using addFields().
     */
    public function testMatchSearchCriterion()
    {
        $field = new ORM_TextField('text');
        $criterion = new ORM_MatchSearchCriterion($field, 'A house of cards', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();
        
        $this->assertCount(2, $keywords);
        $this->assertContains('house', $keywords);
        $this->assertContains('cards', $keywords);

    
        $criterion = new ORM_MatchSearchCriterion($field, 'A "house of cards"', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();
        
        $this->assertCount(1, $keywords);
        $this->assertContains('house of cards', $keywords);

        $criterion = new ORM_MatchSearchCriterion($field, 'Hello, how are you?', '_AND_', 'contains');
        
        $keywords = $criterion->getSearchStringKeywords();
        
        $this->assertCount(4, $keywords);
        $this->assertContains('Hello', $keywords);
        $this->assertContains('how', $keywords);
        $this->assertContains('are', $keywords);
        $this->assertContains('you', $keywords);
        
        
        $criterion = new ORM_MatchSearchCriterion($field, 'Hello, "how are you?"', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();
        
        $this->assertCount(2, $keywords);
        $this->assertContains('Hello', $keywords);
        $this->assertContains('how are you?', $keywords);

        $criterion = new ORM_MatchSearchCriterion($field, 'a "b" c', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();
        
        $this->assertCount(1, $keywords);
        $this->assertContains('b', $keywords);
    }
    
    
}
