<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/operationTest.php';

class ORM_lengthOperationTest extends ORM_operationTest
{
    protected $fieldClass = 'ORM_LengthOperation';


    public function testRealOperation()
    {
        $recordSet = $this->getRecordSet();

        $recordSet->addFields($recordSet->name->length()->setName('testedOperation'));
        $record = $recordSet->newRecord();
        $record->name = 'abcdefgh';
        $record->save();

        $r = $recordSet->get($record->id);

        $this->assertEquals($r->testedOperation, strlen($record->name));
    }
}
