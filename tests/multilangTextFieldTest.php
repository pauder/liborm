<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/textFieldTest.php';

class ORM_multilangTextFieldTest extends ORM_textFieldTest
{
    protected $fieldClass = 'ORM_MultilangTextField';
}
