<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';



class ORM_RecordTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test that using isset() on an existing but uninitialized record property must return false.
     */
    public function testIssetOnExistingButUninitializedRecordProperty()
    {
        $db = new LibOrm_MysqlMockAdapter();
        
        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
        
        $recordSet->addFields(new ORM_IntField('initialized'));
        
        $record = $recordSet->newRecord();
        
        $this->assertFalse(
            isset($record->uninitialized),
            'Using isset() on an existing but uninitialized record property must return false.'
        );
    }


    /**
     * Test that using isset() on an existing and initialized record property must return true.
     */
    public function testIssetOnExistingAndInitializedRecordProperty()
    {
        $db = new LibOrm_MysqlMockAdapter();
        
        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
        
        $recordSet->addFields(new ORM_IntField('initialized'));
        
        $record = $recordSet->newRecord();
        
        $record->initialized = 2;
        
        $this->assertTrue(
            isset($record->initialized),
            'Using isset() on an existing and initialized record property must return true.'
        );
    }


    /**
     * Test that using isset() on an existing and initialized to null record property must return false.
     */
    public function testIssetOnExistingAndInitializedToNullRecordProperty()
    {
        $db = new LibOrm_MysqlMockAdapter();
        
        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
        
        $recordSet->addFields(new ORM_IntField('initialized'));
        
        $record = $recordSet->newRecord();
        
        $record->initialized = null;
        
        $this->assertFalse(
            isset($record->initialized),
            'Using isset() on an existing and initialized record property must return false.'
        );
    }


    /**
     * Test that using isset() on a non-existent record property field must return false.
     */
    public function testIssetOnNonExistentRecordProperty()
    {
        $db = new LibOrm_MysqlMockAdapter();
        
        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
        
        $field1 = new ORM_IntField('uninitialized');
        $field2 = new ORM_IntField('initialized');
        $recordSet->addFields($field1, $field2);
        
        $record = $recordSet->newRecord();
        
        $this->assertFalse(
            isset($record->nonExistent),
            'Using isset() on a non-existent record property field must return false.'
        );
    }


    /**
     * Test accessing a property value from a record using getValue().
     */
    public function testGetSimpleValue()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
         
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
         
        $record = $recordSet->newRecord();
        $record->setValue('name', 'My record name');
         
        $nameValue = $record->getValue('name');
        $inexistentValue = $record->getValue('inexistent');
         
        $this->assertEquals($nameValue, 'My record name');
        $this->assertEquals($nameValue, $record->name);
        
        $this->assertNull($inexistentValue);
    }



    /**
     * Test accessing a property value from a record  using __get().
     */
    public function testGetSimpleValueUsingMagicGetMethod()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
         
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
         
        $record = $recordSet->newRecord();
        $record->setValue('name', 'My record name');

        $nameValue = $record->name;
        $inexistentValue = $record->inexistent;
         
        $this->assertEquals($nameValue, 'My record name');
         
        $this->assertNull($inexistentValue);
    }



//     /**
//      * Test accessing a field from a record set getFieldByPath().
//      */
//     public function testGetFieldUsingGetFieldByPath()
//     {
//         $set = new ORM_TestRecordSet();
//         $field1 = new ORM_StringField('name', 255);

//         $set->addFields($field1);
//         $set->hasOne('otherTest', 'ORM_TestRecordSet');

//         $nameField = $set->getFieldByPath('name');

//         $this->assertEquals($nameField, $field1);
//     }



//     /**
//      * Test accessing a non-existent field from a record set getFieldByPath().
//      *
//      *  @expectedException ORM_OutOfBoundException
//      *                  Accessing a non-existent field on a record set
//      *                  must throw an ORM_OutOfBoundException.
//      */
//     public function testGetInexistentFieldUsingGetFieldByPath()
//     {
//         $set = new ORM_TestRecordSet();
//         $field1 = new ORM_StringField('name', 255);
    
//         $set->addFields($field1);
    
//         $set->getFieldByPath('wrong');
    
//         $nameField = $set->getFieldByPath('name');
    
//         $this->assertEquals($nameField, $field1);
//     }

    /**
     * Test accessing a property value from a record using __call().
     */
    public function testGetSimpleValueUsingMagicCallMethod()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
         
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
         
        $record = $recordSet->newRecord();
        $record->setValue('name', 'My record name');

        $nameValue = $record->name();
         
        $this->assertEquals($nameValue, 'My record name');
    }

    
    /**
     * Test accessing a non-existent propery value from a record using __call().
     *
     * @expectedException ORM_Exception
     *                  Accessing an non-existent propery from a record using __call()
     *                  must throw an ORM_Exception.
     */
    public function testGetInexistentValueUsingMagicCallMethod()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
         
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $recordSet->addFields($field1, $field2);
         
        $record = $recordSet->newRecord();

        $inexistentValue = $record->inexistent();
    }

    
    
    


    /**
     * Test accessing non-joined foreign key on a new record.
     */
    public function testGetNonJoinedForeignKeyValueOnNewRecord()
    {
        $db = new LibOrm_MysqlMockAdapter();

        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
     	$recordSet->setPrimaryKey('id');
     	$recordSet->hasOne('otherTest', 'ORM_TestRecordSet');

     	$record = $recordSet->newRecord();
     	
     	$value = $record->getValue('otherTest');
    	 
     	$this->assertNull($value);
     	
     	$value = $record->otherTest;

       	$this->assertNull($value);

       	$value = $record->otherTest();

       	$this->assertNull($value);
    }


    
    
    /**
     * Test accessing joined foreign key on a new record.
     */
    public function testGetJoinedForeignKeyValueOnNewRecord()
    {
        $db = new LibOrm_MysqlMockAdapter();
        
        $recordSet = new ORM_TestRecordSet();
        $recordSet->setBackend(new ORM_MySqlBackend($db));
        $recordSet->setPrimaryKey('id');
        $recordSet->hasOne('otherTest', 'ORM_TestRecordSet');
    
        $recordSet->join('otherTest');
    
        $record = $recordSet->newRecord();

        
        $value = $record->getValue('otherTest');
        
        $this->assertInstanceOf('ORM_TestRecord', $value);
        
        $value = $record->otherTest;
        
        $this->assertInstanceOf('ORM_TestRecord', $value);
                
        $value = $record->otherTest();
        
        $this->assertInstanceOf('ORM_TestRecord', $value);
    }
    
    

//     /**
//      * Test joining a FkField using the ORM_RecordSet::__call() method.
//      */
//     public function testHasOneJoined2()
//     {
//         $set = $this->testHasOne();
//         $joinedSet = $set->otherTest();
    
//         $field = $set->getField('otherTest');
    
//         $this->assertInstanceOf(
//             'ORM_TestRecordSet',
//             $field,
//             'Joining a FkField using the ORM_RecordSet::__call() method, the joined field must be replaced by the corresponding RecordSet'
//         );
    
//         $this->assertSame(
//             $joinedSet,
//             $field,
//             'Joining a FkField using the ORM_RecordSet::__call() method must return the joined RecordSet'
//         );
//     }


//     /**
//      * Test joining a field that is not a FkField.
//      */
//     public function testJoinOnWrongField()
//     {
//         $set = new ORM_TestRecordSet();
//         $field1 = new ORM_StringField('name', 255);
//         $field2 = new ORM_IntField('age');
//         $set->addFields($field1, $field2);
        
//         $result = $set->join('name');
        
//         $this->assertNull($result);
//     }


//     /**
//      * Test adding primary key using setPrimaryKey().
//      */
//     public function testNewRecord()
//     {
//         $set = new ORM_TestRecordSet();
    
//         $record = $set->newRecord();
         
//         $this->assertInstanceOf('ORM_TestRecord', $record);
//     }

// //     /**
// //      * Test adding primary key using setPrimaryKey().
// //      */
// //     public function testNewRecord()
// //     {
// //         $set = new ORM_TestRecordSet();
// //         $set->setPrimaryKey('id');
    
// //         $idField = $set->getField('id');
         
// //         $this->assertInstanceOf('ORM_PkField', $idField);
    
// //         $pkFieldName = $set->getPrimaryKey();
// //         $this->assertEquals($pkFieldName, 'id');
    
// //         $pkField = $set->getPrimaryKeyField();
// //         $this->assertSame($pkField, $idField);
    
// //         //     	$record = $set->newRecord();
         
// //         //     	$this->assertInstanceOf('ORM_TestRecord', $record);
         
// //         //     	$values = $record->getValues();
// //         //     	$this->assertInternalType('array', $values);
         
// //         //     	$this->assertArrayHasKey('id', $values);
// //     }

    

//     /**
//      * Test setting a backend.
//      */
//     public function testSetBackend()
//     {
//         $mockBackend = $this->getMock('ORM_Backend');
        
//         ORM_TestRecordSet::setBackend($mockBackend);
    
//         $backend = ORM_TestRecordSet::getBackend();
    
//         $this->assertSame($backend, $mockBackend);
//     }
}
