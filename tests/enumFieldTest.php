<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/fieldTest.php';

class ORM_enumFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_EnumField';


    /**
     * @return ORM_Field
     */
    protected function construct()
    {
        require_once dirname(__FILE__) . '/../programs/field.class.php';
        $args = func_get_args();
        if (!isset($args[1])) {
            $args[1] = array(1 => 'Value 1', 2 => 'Value 2');
        }
        $field = $this->getMockForAbstractClass($this->fieldClass, $args);
        return $field;
    }
}
