<?php

require_once dirname(__FILE__) . '/../programs/orm.class.php';
require_once dirname(__FILE__) . '/../programs/MySql/adapter.class.php';



$GLOBALS['babInstallPath'] = 'vendor/ovidentia/ovidentia/ovidentia/';


class ORM_TestRecordCreateSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
        );
    }
}



class ORM_TestRecordCreate extends ORM_Record
{
}


class ORM_RecordCreateTest extends PHPUnit_Framework_TestCase
{

   protected function getRecordSet()
   {
       global $babDB;

       $ORM = new Func_LibOrm;
       $ORM->initMysql();

       $babDB = new LibOrm_MysqlAdapter('localhost', 'test', '', 'test');
       $backend = new ORM_MySqlBackend($babDB);

       $set = new ORM_TestRecordCreateSet();
       $set->setBackend($backend);

       return $set;
   }


   public function setUp()
   {
       global $babDB;
       $babDB->db_query('DROP TABLE IF EXISTS orm_testrecordcreate');

       $recordSet = $this->getRecordSet();

       require_once dirname(__FILE__).'/../vendor/ovidentia/ovidentia/ovidentia/utilit/devtools.php';

       $sql = new bab_synchronizeSql();
       $sql->addOrmSet($recordSet);

       $sql->updateDatabase();
   }

   public function tearDown()
   {
       global $babDB;
       $babDB->db_query('DROP TABLE IF EXISTS orm_testrecordcreate');
   }


   public function testCreate()
   {
       $recordSet = $this->getRecordSet();
       $record = $recordSet->newRecord();

       $record->name = 'test';
       $this->assertTrue($record->save());

       $record2 = $recordSet->get($record->id);

       $this->assertInstanceOf(ORM_Record::class, $record2);
       $this->assertEquals($record->id, $record2->id);
   }


   public function testCreateWithPrimaryKeySpecified()
   {
       $recordSet = $this->getRecordSet();
       $record = $recordSet->newRecord();

       $record->id = 123;
       $record->name = 'test';
       $this->assertTrue($record->save());

       $record->name = 'test 2';
       $this->assertTrue($record->save());

       $this->assertEquals('123', $record->id);

       $record2 = $recordSet->get($record->id);

       $this->assertInstanceOf(ORM_Record::class, $record2);
       $this->assertEquals($record->id, $record2->id);
   }


   public function testSettingPrimaryKeyWithSameValue()
   {
       $recordSet = $this->getRecordSet();
       $record = $recordSet->newRecord();

       $record->name = 'test';
       $this->assertTrue($record->save());

       $record->id = $record->id;
       $this->assertTrue($record->save());
   }
}
