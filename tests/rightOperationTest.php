<?php

require_once dirname(__FILE__).'/mock/mockObjects.php';
require_once dirname(__FILE__).'/operationTest.php';

class ORM_rightOperationTest extends ORM_operationTest
{
    protected $fieldClass = 'ORM_RightOperation';

    protected function getRecordSet()
    {
        $set = parent::getRecordSet();

        $set->addFields($set->name->right(3)->setName('testedOperation'));

        return $set;
    }


    public function testRealOperation()
    {
        $recordSet = $this->getRecordSet();
        $record = $recordSet->newRecord();
        $record->name = 'abcdefgh';
        $record->save();

        $r = $recordSet->get($record->id);

        $this->assertEquals($r->testedOperation, substr($record->name, -3));
    }
}
