<?php

require_once dirname(__FILE__) . '/../programs/orm.class.php';
require_once dirname(__FILE__) . '/../programs/MySql/adapter.class.php';



$GLOBALS['babInstallPath'] = 'vendor/ovidentia/ovidentia/ovidentia/';


class ORM_TestRecordDeleteSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
    
        $this->setPrimaryKey('id');
    
        $this->addFields(
            ORM_StringField('name')
        );

    }
    
}

class ORM_TestRecordDelete extends ORM_Record
{
}


class ORM_RecordDeleteTest extends PHPUnit_Framework_TestCase
{
    
   protected function getSet()
   {
       global $babDB;
       
       $ORM = new Func_LibOrm;
       $ORM->initMysql();
       
       $babDB = new LibOrm_MysqlAdapter('localhost', 'test', '', 'test');
       $backend = new ORM_MySqlBackend($babDB);
       
       $set = new ORM_TestRecordDeleteSet();
       $set->setBackend($backend);
       
       return $set;
   }
   
   
   public function testCreateTable()
   {
       $set = $this->getSet();
       
       require_once dirname(__FILE__).'/../vendor/ovidentia/ovidentia/ovidentia/utilit/devtools.php';
       
       $sql = new bab_synchronizeSql();
       $sql->addOrmSet($set);
       
       $sql->updateDatabase();
       
       $this->assertTrue($sql->isCreatedTable('orm_testrecorddelete'));
   }
   
   
   public function testSave()
   {
       $set = $this->getSet();
       $record = $set->newRecord();
       
       $record->name = 'test';
       $this->assertTrue($record->save());
       
       $this->assertEquals('1', $record->id); // AUTO INCREMENT should return 1 with new table ?
   }
   
   
   public function testDeleteRecord()
   {
       $set = $this->getSet();
       $record = $set->get(1);
       
       $this->assertInstanceOf('ORM_TestRecordDelete', $record);
       $this->assertTrue($record->delete());
       
       $this->assertEquals(null, $set->get(1));
   }
   
   public function testDeleteTable()
   {
       global $babDB;
       $babDB->db_query('DROP TABLE orm_testrecorddelete');
   }
}