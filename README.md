## Interface avec la base de données MYSQL ##

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/liborm/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/liborm/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/liborm/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/liborm/?branch=master)

Bibliothèque de correspondance entre base de données et objets PHP (mapping objet-relationel)