<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


DEFINE('ORM_EOF', - 1);

/**
 * An iterator on ORM results.
 * ORM_Iterator object are not created directly but are returned
 * by a call to ORM_RecordSet::select().
 *
 * @see ORM_RecordSet::select()
 */
abstract class ORM_Iterator implements SeekableIterator
{

    /**
     * The RecordSet associated to the iterator.
     *
     * @var ORM_RecordSet
     */
    protected $oSet = null;

    /**
     * The Criteria associated to the iterator.
     *
     * @var ORM_Criteria
     */
    protected $oCriteria = null;

    /**
     * The fields that will be used to order the results.
     * Each element of the array contains an ORM_Field and a string defining
     * the direction ('ASC' or 'DESC').
     *
     * @var array(array(ORM_Field, string))
     */
    protected $aOrder = array();

    /**
     * The limit that will be used like mysql limit.
     *
     * @var string
     */
    protected $sLimit = null;

    /**
     * The fields that will be used to group the results.
     *
     * @var array(ORM_Field)
     */
    protected $aGroupBy = array();

    /**
     * The iterator constructor.
     */
    public function __construct()
    {
    }

    /**
     * Sets the RecordSet associated to the iterator.
     *
     * @param ORM_RecordSet $oSet
     * @return ORM_Iterator
     */
    public function setSet(ORM_RecordSet $oSet)
    {
        $this->oSet = $oSet;
        return $this;
    }

    /**
     * Returns the RecordSet associated to the iterator.
     *
     * @return ORM_RecordSet
     */
    public function getSet()
    {
        return $this->oSet;
    }

    /**
     * Adds an ascending orderby field to the iterator.
     *
     * @param ORM_Field $oField
     * @return ORM_Iterator
     */
    public function orderAsc(ORM_Field $oField)
    {
        $this->aOrder[] = array(
            $oField,
            'ASC'
        );
        return $this;
    }

    /**
     * Adds a descending orderby field to the iterator.
     *
     * @param ORM_Field $oField
     * @return ORM_Iterator
     */
    public function orderDesc(ORM_Field $oField)
    {
        $this->aOrder[] = array(
            $oField,
            'DESC'
        );
        return $this;
    }

    /**
     * Set the order in random mod.
     *
     * @since 0.9.7
     *
     * @return ORM_Iterator
     */
    public function orderRand()
    {
        $this->aOrder[] = array(
            'RAND()'
        );
    }


    /**
     * Adds a mysql like limit to the iterator.
     *
     * @since 0.9.6
     *
     * @param string $limit
     * @return ORM_Iterator
     */
    public function limit($limit)
    {
        $this->sLimit = $limit;
        return $this;
    }

    /**
     * Adds a groupby field to the iterator.
     *
     * @param ORM_Field $oField
     * @return ORM_Iterator
     */
    public function groupBy(ORM_Field $oField)
    {
        $this->aGroupBy[] = $oField;
        return $this;
    }

    /**
     * Sets the criteria associated to the iterator.
     * This function should not be called directly use ORM_RecordSet::select() instead.
     *
     * @see ORM_RecordSet::select()
     *
     * @param ORM_Criteria $oCriteria
     * @return ORM_Iterator
     */
    public function setCriteria(ORM_Criteria $oCriteria = null)
    {
        $this->oCriteria = $oCriteria;
        return $this;
    }

    /**
     * Returns the criteria associated to the iterator.
     *
     * @return ORM_Criteria
     */
    public function getCriteria()
    {
        if (is_null($this->oCriteria)) {
            $this->oCriteria = new ORM_Criteria();
        }
        return $this->oCriteria;
    }

    /**
     * Break links to other objects before an unlink on the iterator.
     */
    protected function freeOrmLinks()
    {
        $this->oSet = null;
        $this->oCriteria = null;
        $this->aOrder = array();
        $this->aGroupBy = array();
        $this->sLimit = null;
    }

    /**
     * Destruct the iterator.
     */
    public function __destruct()
    {
        $this->freeOrmLinks();
    }
}

