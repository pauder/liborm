<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
 ************************************************************************/


/**
 * Database adapter for the mysql backend.
 * This class is compatible with babDatabase from ovidentia core
 *
 * This can be used instead of $babDB for the unit tests of ORM classes
 */
class LibOrm_MysqlAdapter
{
    /**
     * @var string
     */
	private $host;

	/**
	 * @var string
	 */
	private $login;

	/**
	 * @var string
	 */
	private $password;

	/**
	 * @var string
	 */
	private $dbname;

	/**
	 * @var mysqli
	 */
	private $dblink;

    /**
     * @param string $host database server host name
     * @param string $login
     * @param string $password
     * @param string $dbname Database name
     */
	public function __construct($host, $login, $password, $dbname)
	{
		$this->host 	= $host;
		$this->login 	= $login;
		$this->password = $password;
		$this->dbname 	= $dbname;
	}

	/**
	 * Connect
	 */
    protected function connect()
    {
        if (! isset($this->dblink)) {
            $this->dblink = mysqli_connect($this->host, $this->login, $this->password);
            if ($this->dblink) {
                $res = mysqli_select_db($this->dblink, $this->dbname);
                if ($res == false) {
                    die("Cannot select database : " . $this->dbname);
                }
            } else {
                die("Cannot connect to database : " . $this->dbname);
            }
        }

        return $this->dblink;
    }

    /**
     * Print error
     * @param string $text
     * @return string
     */
    public function db_print_error($text)
    {
        $str = "<h2>" . $text . "</h2>\n";
        $str .= "<p><b>Database Error: ";
        $str .= $this->db_error();
        $str .= "</b></p>\n";

        $displayErrors = (int) ini_get('display_errors');
        $errorReporting = (int) ini_get('error_reporting');
        if (E_USER_ERROR === ($errorReporting & E_USER_ERROR) && $displayErrors) {
            echo $str;
        }

        return $str;
    }


    /**
     * Close database conexion
     */
	public function db_close()
	{
		return mysqli_close($this->connect());
	}

    /**
     * Set mysql connexion charset according to the charset of the database
     */
    public function db_setCharset()
    {
        $oResult = $this->db_query("SHOW VARIABLES LIKE 'character_set_database'");
        if (false !== $oResult) {
            $aDbCharset = $this->db_fetch_assoc($oResult);
            if (false !== $aDbCharset && 'utf8' == $aDbCharset['Value']) {
                $this->db_query("SET NAMES utf8");
                return;
            }
        }

        $this->db_query("SET NAMES latin1");
    }


    /**
     * Create a database
     * @param string $dbname
     * @return resource
     */
	public function db_create_db($dbname)
	{
		return $this->db_query('CREATE DATABASE '.$this->backTick($dbname));
	}

	/**
	 * Drop a database
	 * @param string $dbname
	 * @return resource
	 */
	public function db_drop_db($dbname)
	{
		return $this->db_query('DROP DATABASE '.$this->backTick($dbname));
	}

    /**
     * sends an unique query (multiple queries are not supported)
     *
     * @param string $query
     * @return resource|false
     */
    public function db_query($query)
    {
        $res = mysqli_query($this->connect(), $query);
        if (! $res) {
            $this->db_print_error("Can't execute query : <br><pre>" . htmlspecialchars($query) . "</pre>");
        }

        return $res;
    }

    /**
     * @param resource $result
     * @return int
     */
	public function db_num_rows($result)
	{
		if (!$result) {
			return 0;
		}
		return mysqli_num_rows($result);
	}

	/**
	 * @param resource $result
	 * @return array
	 */
	public function db_fetch_array($result)
    {
        $arr = mysqli_fetch_array($result);
        if (null === $arr) {
            return false;
        }
        return $arr;
    }

    /**
     * @param resource $result
     * @return array
     */
	public function db_fetch_assoc($result)
    {
        $arr = mysqli_fetch_assoc($result);
        if (null === $arr) {
            return false;
        }
        return $arr;
    }

    /**
     * @param resource $result
     * @return array
     */
	function db_fetch_row($result)
	{
		return mysqli_fetch_row($result);
	}

	/**
	 * @return int
	 */
	public function db_affected_rows()
	{
		return mysqli_affected_rows($this->connect());
	}

	/**
	 * last auto increment value
	 * @return int
	 */
	public function db_insert_id()
	{
		return mysqli_insert_id($this->connect());
	}

	/**
	 * @param resource $res result
     * @param int $row offset
	 * @return mixed
	 */
	public function db_data_seek($res, $row)
	{
		return mysqli_data_seek($res, $row);
	}

	/**
	 * @param string $str
	 * @return string
	 */
	public function db_escape_string($str)
	{
		return mysqli_real_escape_string($this->connect(), $str);
	}

    /**
     * Special chars for LIKE query
     *
     * @param string $str
     * @return string
     */
    public function db_escape_like($str)
    {
        $str = str_replace('\\', '\\\\', $str);
        $str = str_replace('%', '\%', $str);
        $str = str_replace('?', '\?', $str);
        return $this->db_escape_string($str);
    }

    /**
     * Encode array or string for query and add quotes
     *
     * @param array|string $param
     * @return string
     */
    public function quote($param)
    {
        if (is_array($param)) {

            foreach ($param as &$value) {
                $value = $this->db_escape_string($value);
            }
            unset($value);

            return "'" . implode("','", $param) . "'";
        } else {
            return "'" . $this->db_escape_string($param) . "'";
        }
    }

	/**
	 * Encode array or string for query and add quotes or if the value is NULL,
	 * return the NULL string.
	 *
	 * @since 7.1.94
	 *
	 * @param	array|string|null	$param
	 * @return	string
	 */
	public function quoteOrNull($param)
	{
		if (null === $param) {
			return 'NULL';
		}

		return $this->quote($param);
	}


	/**
	 * Adds backticks (`) to an SQL identifier (database, table or column name).
	 * @see http://dev.mysql.com/doc/refman/4.1/en/identifiers.html
	 * @since	6.4.95
	 * @param	string	$identifier
	 * @return	string	The backticked identifier.
	 */
	public function backTick($identifier)
	{
		// Backticks are allowed in an identifier but should be backticked.
		$identifier = '`' . str_replace('`', '``', $identifier) . '`';

		return $identifier;
	}



	/**
	 * Get error info
	 * return false if no error on the last query
	 * return the error string if error on the last query
	 * @since	6.4.95
	 * @return 	false|string
	 */
	public function db_error()
	{
		$error = mysqli_error($this->connect());
		return empty($error) ? false : $error;
	}

    /**
     * Query without error manager
     *
     * @since 6.4.95
     * @param string $query
     * @return resource|false
     */
    public function db_queryWem($query)
    {
        return mysqli_query($this->connect(), $query);
    }
}
