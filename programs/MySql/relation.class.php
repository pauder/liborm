<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/../field.class.php';
require_once dirname(__FILE__) . '/../criteria.class.php';


/**
 * One relation
 */
class ORM_OneRelation
{
	private $sOwnerSetClassName		= null;
	private $sOwnerSetFieldName		= null;
	private $sForeignSetClassName	= null;

	/**
	 * @param string $sOwnerSetClassName
	 * @param string $sOwnerSetFieldName
	 * @param string $sForeignSetClassName
	 */
	public function __construct($sOwnerSetClassName, $sOwnerSetFieldName, $sForeignSetClassName)
	{
		$this->setOwnerSetClassName($sOwnerSetClassName);
		$this->setOwnerSetFieldName($sOwnerSetFieldName);
		$this->setForeignSetClassName($sForeignSetClassName);
	}

	/**
	 * @return string
	 */
	public function getOwnerSetClassName()
	{
		return $this->sOwnerSetClassName;
	}

	/**
	 * @param string $sOwnerSetClassName
	 */
	public function setOwnerSetClassName($sOwnerSetClassName)
	{
		$this->sOwnerSetClassName = $sOwnerSetClassName;
	}

	/**
	 * @return string
	 */
	public function getOwnerSetFieldName()
	{
		return $this->sOwnerSetFieldName;
	}

	/**
	 * @param string $sOwnerSetFieldName
	 */
	public function setOwnerSetFieldName($sOwnerSetFieldName)
	{
		$this->sOwnerSetFieldName = $sOwnerSetFieldName;
	}

	/**
	 * @return string
	 */
	public function getForeignSetClassName()
	{
		return $this->sForeignSetClassName;
	}

	/**
	 * @param string $sForeignSetClassName
	 */
	public function setForeignSetClassName($sForeignSetClassName)
	{
		$this->sForeignSetClassName	= $sForeignSetClassName;
	}
}

/**
 * Many relation
 */
class ORM_ManyRelation extends ORM_OneRelation
{
	private $sForeignFieldName = null;

	private $sRelationSetClassName = null;

	private $relationSet = null;


	const ON_DELETE_CASCADE = 'cascade';
	const ON_DELETE_SET_NULL = 'null';
	const ON_DELETE_NO_ACTION = 'no action';

	/**
	 * The action to perform of the foreign records when a record is deleted
	 *
	 * @var string
	 */
	private $onDeleteMethod = self::ON_DELETE_NO_ACTION;



	/**
	 * @param string	$sOwnerSetClassName
	 * @param string	$sOwnerSetRelationName
	 * @param string	$sForeignSetClassName
	 * @param string	$sForeignFieldName
	 * @param string	$sRelationSetClassName		The name of the RecordSet corresponding to the relation.
	 */
	public function __construct($sOwnerSetClassName, $sOwnerSetRelationName, $sForeignSetClassName, $sForeignFieldName = null, $sRelationSetClassName = null)
	{
		parent::__construct($sOwnerSetClassName, $sOwnerSetRelationName, $sForeignSetClassName);
		$this->setForeignFieldName($sForeignFieldName);
		$this->setRelationSetClassName($sRelationSetClassName);
	}


	/**
	 * @param string $method
	 *
	 * @return ORM_ManyRelation
	 */
	public function setOnDeleteMethod($method)
	{
		$this->onDeleteMethod = $method;

		return $this;
	}


	/**
	 * @return string
	 */
	public function getOnDeleteMethod()
	{
		return $this->onDeleteMethod;
	}


	/**
	 * @return string
	 */
	public function getForeignFieldName()
	{
		return $this->sForeignFieldName;
	}


	/**
	 * @param string	$sForeignFieldName
	 * @return ORM_ManyRelation
	 */
	public function setForeignFieldName($sForeignFieldName)
	{
		$this->sForeignFieldName = $sForeignFieldName;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getRelationSetClassName()
	{
		return $this->sRelationSetClassName;
	}


	/**
	 * @param string	$sRelationSetClassName
	 * @return ORM_ManyRelation
	 */
	public function setRelationSetClassName($sRelationSetClassName)
	{
		$this->sRelationSetClassName = $sRelationSetClassName;
		return $this;
	}



	/**
	 *
	 * @return ORM_RecordSet
	 */
	public function getSet()
	{
		if (!isset($this->relationSet)) {
			$relationSetClassName = $this->getRelationSetClassName();
			$this->relationSet = new $relationSetClassName;
		}

		return $this->relationSet;
	}

	/**
	 * @param int $id
	 *
	 * @return ORM_Criterion
	 */
	public function isLinkedWith($id)
	{
		$relationSet = $this->getSet();

		$foreignFieldName = $this->getForeignFieldName();

		$foreignField = $relationSet->$foreignFieldName;

		return $foreignField->is($id);
	}
}





class ORM_Relations
{
	private $aManyRelations	= array();
	private $aOneRelations	= array();


	/**
	 * @param ORM_OneRelation $oRelation
	 *
	 * @return ORM_Relations
	 */
	public function addRelation(ORM_OneRelation $oRelation)
	{
		$sOwnerSetClassName = $oRelation->getOwnerSetClassName();
		if ($oRelation instanceof ORM_ManyRelation) {
			$this->aManyRelations[$sOwnerSetClassName][$oRelation->getOwnerSetFieldName()] = $oRelation;
		} elseif ($oRelation instanceof ORM_OneRelation) {
			$this->aOneRelations[$sOwnerSetClassName][$oRelation->getOwnerSetFieldName()] = $oRelation;
		}

		return $this;
	}


	/**
	 * @param string	$sOwnerSetClassName
	 * @param string	$sOwnerSetFieldName
	 *
	 * @return ORM_OneRelation
	 */
	public function getRelation($sOwnerSetClassName, $sOwnerSetFieldName)
	{
		if (isset($this->aOneRelations[$sOwnerSetClassName][$sOwnerSetFieldName])) {
			return $this->aOneRelations[$sOwnerSetClassName][$sOwnerSetFieldName];
		}

		if (isset($this->aManyRelations[$sOwnerSetClassName][$sOwnerSetFieldName])) {
			return $this->aManyRelations[$sOwnerSetClassName][$sOwnerSetFieldName];
		}

		return null;
	}


	/**
	 * Get list of has one relations
	 * @return array
	 */
	public function getHasOneRelations($sOwnerSetClassName)
	{
		if (!isset($this->aOneRelations[$sOwnerSetClassName])) {
			return array();
		}

		return $this->aOneRelations[$sOwnerSetClassName];
	}


	/**
	 * Get list of has many relations
	 * @return array
	 */
	public function getHasManyRelations($sOwnerSetClassName)
	{
		if (!isset($this->aManyRelations[$sOwnerSetClassName])) {
			return array();
		}

		return $this->aManyRelations[$sOwnerSetClassName];
	}
}
