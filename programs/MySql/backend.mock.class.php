<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/backend.class.php';
require_once dirname(__FILE__) . '/iterator.mock.class.php';



/**
 * MySql mock backend for test suites
 * add functionality to set the return values for the select, save methods
 */
class ORM_MySqlMockBackend extends ORM_MySqlBackend
{
	private $lastSelectCriteria = array();
    
    private $selectIterator = array();
	
	private $saveReturnValue = array();
	
	private $saveRecordUpdate = array();

    /**
     * Set a list of record to be returned by the next calls to the select method
     * 
     * @param ORM_RecordSet $set            
     * @param array $records            
     * @return ORM_MySqlMockBackend
     */
    public function setSelectReturn(ORM_RecordSet $set, ORM_Criteria $criteria, array $records)
    {
        if (! isset($this->selectIterator[get_class($set)])) {
            $this->selectIterator[get_class($set)] = array();
        }
        
        $this->selectIterator[get_class($set)][] = array(
            'criteria' => $criteria,
            'iterator' => new ORM_MySqlMockIterator($records)
        );
        return $this;
    }

    /**
     * (non-PHPdoc)
     * 
     * @see ORM_MySqlBackend::select()
     */
    public function select(ORM_RecordSet $set, ORM_Criteria $criteria = null)
    {
        $this->lastSelectCriteria[get_class($set)] = $criteria;
        
        if (! isset($this->selectIterator[get_class($set)])) {
            return new ORM_MySqlMockIterator(array());
        }
        
        foreach ($this->selectIterator[get_class($set)] as $arr) {
            if ($arr['criteria']->toString($this) === $criteria->toString($this)) {
                return $arr['iterator'];
            }
        }
        
        return new ORM_MySqlMockIterator(array());
    }
    
    /**
     * Get the last criteria received by the select method
     * @return ORM_Criteria
     */
    public function getLastSelectCriteria(ORM_RecordSet $set)
    {
        return $this->lastSelectCriteria[get_class($set)];
    }
    

    /**
     * Set a return values for the next call to the save method
     * The second parameter can be used tu update the value of the record, ex: to set the id
     */
    public function setSaveReturn(ORM_RecordSet $set, $returnValue, array $recordUpdate = null)
    {
        $this->saveReturnValue[get_class($set)] = $returnValue;
        $this->saveRecordUpdate[get_class($set)] = $recordUpdate;
        
        return $this;
    }

    /**
     * (non-PHPdoc)
     * 
     * @see ORM_MySqlBackend::save()
     */
    public function save(ORM_RecordSet $set, ORM_Record $oRecord)
    {
        if (isset($this->saveRecordUpdate[get_class($set)])) {
            foreach ($this->saveRecordUpdate[get_class($set)] as $field => $value) {
                $oRecord->$field = $value;
            }
        }
        
        if (isset($this->saveReturnValue[get_class($set)])) {
            return $this->saveReturnValue[get_class($set)];
        }
        
        return true;
    }
}
